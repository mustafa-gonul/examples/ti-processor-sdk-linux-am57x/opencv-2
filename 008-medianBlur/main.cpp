#include <opencv2/opencv.hpp>
#include "ocl_util.h"
#include <limits>
#include <time.h>
#include <unistd.h>


using namespace cv;

using Pixel = unsigned char;

/* Time difference calculation, in ms units */
double tdiff_calc(struct timespec &tp_start, struct timespec &tp_end)
{
  return (double)(tp_end.tv_nsec -tp_start.tv_nsec) * 0.000001 + (double)(tp_end.tv_sec - tp_start.tv_sec) * 1000.0;
}

bool checkZero(const Pixel* buffer, int size)
{
  for (int i = 0; i < size; ++i) {
    if (buffer[i]) {
      return false;
    }
  }

  return true;
}

void writeZero(Pixel* buffer, int size)
{
  for (int i = 0; i < size; ++i) {
    buffer[i] = 0;
  }
}

extern "C" {

void heap_init_ddr(char* p, size_t bytes);
void heap_init_msmc(char *p, size_t bytes);

}

int main(int argc, const char** argv)
{
  //
  int width = 21;
  int height = 21;

  if (argc != 3) {
    std::cout << "Invalid parameters!" << std::endl;

    return -1;
  }

  try {
    width = std::stoi(argv[1]);
    height = std::stoi(argv[2]);

    std::cout << "Width = " << width << std::endl;
    std::cout << "Height = " << height << std::endl;
  }
  catch (const std::exception& e) {
    std::cerr << e.what() << '\n';
    return -2;
  }

  int inputWidth = width;
  int inputHeight = height;
  int outputWidth = width;
  int outputHeight = height;

  int inputSize = inputWidth * inputHeight;
  int outputSize = outputWidth * outputHeight;

  auto inputBuffer = new Pixel[inputSize]{0};
  auto outputBuffer = new Pixel[outputSize]{0};
  writeZero(inputBuffer, inputSize);
  writeZero(outputBuffer, outputSize);

  if (checkZero(outputBuffer, outputSize)) {
    printf("Zero, Ok\n");
  }
  else {
    printf("Not Zero, Not Ok!\n");
    return -1;
  }
  
  struct timespec tp0, tp1, tp2, tp3;

  Mat input(inputHeight, inputWidth, CV_8UC1, inputBuffer);
  Mat output(outputHeight, outputWidth, CV_8UC1, outputBuffer);

  randu(input, Scalar::all(std::numeric_limits<Pixel>::min()), Scalar::all(std::numeric_limits<Pixel>::max()));

  UMat in = input.getUMat(ACCESS_READ);
  UMat out = output.getUMat(ACCESS_WRITE);
  // UMat in;
  //UMat out;

  // Median filter
  /*
  void cv::medianBlur(InputArray  src,
                      OutputArray dst,
                      int         ksize
  )
  */

  // -----

  clock_gettime(CLOCK_MONOTONIC, &tp0);

  // 3 - in copy
  // input.copyTo(in);

  clock_gettime(CLOCK_MONOTONIC, &tp1);

  // 3 - median
  medianBlur(in, out, 3);

  clock_gettime(CLOCK_MONOTONIC, &tp2);

  // 3 - out copy
  //out.copyTo(output);

  clock_gettime(CLOCK_MONOTONIC, &tp3);

  printf("---\n");
  printf("medianBlur in copy   -> tdiff=%lf ms \n", tdiff_calc(tp0, tp1));
  printf("medianBlur 3x3       -> tdiff=%lf ms \n", tdiff_calc(tp1, tp2));
  printf("medianBlur out copy  -> tdiff=%lf ms \n", tdiff_calc(tp2, tp3));

  if (checkZero(outputBuffer, outputSize)) {
    printf("Zero, Not Ok!\n");
    return -1;
  }
  else {
    printf("Not Zero, Ok\n");
  }

  // -----

  clock_gettime(CLOCK_MONOTONIC, &tp0);

  // 7 - in copy
  // input.copyTo(in);

  clock_gettime(CLOCK_MONOTONIC, &tp1);

  // 7 - median
  medianBlur(in, out, 7);

  clock_gettime(CLOCK_MONOTONIC, &tp2);

  // 7 - out copy
  // out.copyTo(output);

  clock_gettime(CLOCK_MONOTONIC, &tp3);

  printf("---\n");
  printf("medianBlur in copy   -> tdiff=%lf ms \n", tdiff_calc(tp0, tp1));
  printf("medianBlur 7x7       -> tdiff=%lf ms \n", tdiff_calc(tp1, tp2));
  printf("medianBlur out copy  -> tdiff=%lf ms \n", tdiff_calc(tp2, tp3));

  if (checkZero(outputBuffer, outputSize)) {
    printf("Zero, Not Ok!\n");
    return -1;
  }
  else {
    printf("Not Zero, Ok\n");
  }

  // -----

  printf("---\n");

  return 0;
}
