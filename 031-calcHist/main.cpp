#include <opencv2/opencv.hpp>
#include <limits>
#include <time.h>
#include <unistd.h>


using namespace cv;

using Pixel = unsigned char;

/* Time difference calculation, in ms units */
double tdiff_calc(struct timespec &tp_start, struct timespec &tp_end)
{
  return (double)(tp_end.tv_nsec -tp_start.tv_nsec) * 0.000001 + (double)(tp_end.tv_sec - tp_start.tv_sec) * 1000.0;
}

int main(int argc, const char** argv)
{
  int width = 21;
  int height = 21;

  if (argc != 3) {
    std::cout << "Invalid parameters!" << std::endl;

    return -1;
  }

  try {
    width = std::stoi(argv[1]);
    height = std::stoi(argv[2]);

    std::cout << "Width = " << width << std::endl;
    std::cout << "Height = " << height << std::endl;
  }
  catch (const std::exception& e) {
    std::cerr << e.what() << '\n';
    return -2;
  }

  int inputWidth = width;
  int inputHeight = height;
  int outputWidth = width;
  int outputHeight = height;

  int inputSize = inputWidth * inputHeight;
  int outputSize = outputWidth * outputHeight;

  auto inputBuffer = new char[inputSize];
  auto outputBuffer = new char[outputSize];

  struct timespec tp0, tp1, tp2;

  Mat input(inputHeight, inputWidth, CV_8UC1, inputBuffer);
  Mat output(outputHeight, outputWidth, CV_8UC1, outputBuffer);
  //Mat histogram;

  randu(input, Scalar::all(std::numeric_limits<Pixel>::min()), Scalar::all(std::numeric_limits<Pixel>::max()));

  UMat in;
  UMat out;
  UMat histogram;

  input.copyTo(in);
  output.copyTo(out);

  // Equalize Histogram
  /*
  void cv::equalizeHist(InputArray  src,
                        OutputArray dst
  )
  */

  // calc Hist
  // std::vector<Mat> images = {input};
  std::vector<UMat> images = {in};
  std::vector<int> channels = {0};
  std::vector<int> sizes = {256};
  std::vector<float> ranges = {0, 256};

  clock_gettime(CLOCK_MONOTONIC, &tp0);

  calcHist(images, channels, Mat(), histogram, sizes, ranges);

  clock_gettime(CLOCK_MONOTONIC, &tp1);

  printf("equalizeHist   -> tdiff=%lf ms \n", tdiff_calc(tp0, tp1));

  return 0;
}
