#include <opencv2/opencv.hpp>
#include <limits>
#include <time.h>
#include <unistd.h>


using namespace cv;

using Pixel = unsigned char;

/* Time difference calculation, in ms units */
double tdiff_calc(struct timespec &tp_start, struct timespec &tp_end)
{
  return (double)(tp_end.tv_nsec -tp_start.tv_nsec) * 0.000001 + (double)(tp_end.tv_sec - tp_start.tv_sec) * 1000.0;
}

int main(int argc, const char** argv)
{
  if (argc != 2) {
    std::cout << "Invalid parameters!" << std::endl;

    return -1;
  }

  auto image = imread(argv[1], IMREAD_GRAYSCALE);
  if (image.empty()) {
    std::cout << "No file!" << std::endl;

    return -1;
  }

  std::cout << "Width: " << image.cols << std::endl;
  std::cout << "Height: " << image.rows << std::endl;


  struct timespec tp0, tp1, tp2;

  Mat input = image.clone();
  Mat output = image.clone();
  UMat in;
  UMat out;

  input.copyTo(in);
  output.copyTo(out);

  setNumThreads(0);

  Ptr<FastFeatureDetector> detector = FastFeatureDetector::create();
  std::vector<KeyPoint> keypoints;

  clock_gettime(CLOCK_MONOTONIC, &tp0);

  detector->detect(in, keypoints, UMat());

  clock_gettime(CLOCK_MONOTONIC, &tp1);

  drawKeypoints(in, keypoints, out);

  printf("fastFeatureDetector   -> tdiff=%lf ms \n", tdiff_calc(tp0, tp1));

  return 0;
}
