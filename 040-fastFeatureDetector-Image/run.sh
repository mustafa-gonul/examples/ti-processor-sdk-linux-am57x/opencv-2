#!/bin/bash

NAME=$(basename $(pwd))

# FILES=(
#   "files/lenna.png" 
#   "files/sudoku.jpg"
# )

FILES=($(ls ./files/*))


export TI_OCL_KEEP_FILES=Y
export TI_OCL_LOAD_KERNELS_ONCHIP=Y
export TI_OCL_CACHE_KERNELS=Y
export TI_OCL_VERBOSE_ERROR=Y


for FILE in ${FILES[@]}; do

  export OPENCV_OPENCL_DEVICE='TI AM57:ACCELERATOR:TI Multicore C66 DSP'

  echo ""
  echo "======================================================================================================"
  echo "First Pass (Building) - OpenCL on, Exe: $NAME, FILE: $FILE"
  echo "======================================================================================================"
  ./"$NAME" $FILE
  echo ""

  echo ""
  echo "======================================================================================================"
  echo "Second Pass (Not building) - OpenCL on, Exe: $NAME, FILE: $FILE"
  echo "======================================================================================================"
  ./"$NAME" $FILE
  echo ""

  export OPENCV_OPENCL_DEVICE='disabled'

  echo ""
  echo "======================================================================================================"
  echo "OpenCL off, Exe: $NAME, FILE: $FILE"
  echo "======================================================================================================"
  ./"$NAME" $FILE
  echo ""

done
