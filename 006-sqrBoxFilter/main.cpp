#include <opencv2/opencv.hpp>
#include <limits>
#include <time.h>
#include <unistd.h>


using namespace cv;

using Pixel = unsigned char;

/* Time difference calculation, in ms units */
double tdiff_calc(struct timespec &tp_start, struct timespec &tp_end)
{
  return (double)(tp_end.tv_nsec -tp_start.tv_nsec) * 0.000001 + (double)(tp_end.tv_sec - tp_start.tv_sec) * 1000.0;
}

int main(int argc, const char** argv)
{
  int width = 21;
  int height = 21;

  if (argc != 3) {
    std::cout << "Invalid parameters!" << std::endl;

    return -1;
  }

  try {
    width = std::stoi(argv[1]);
    height = std::stoi(argv[2]);

    std::cout << "Width = " << width << std::endl;
    std::cout << "Height = " << height << std::endl;
  }
  catch (const std::exception& e) {
    std::cerr << e.what() << '\n';
    return -2;
  }

  int inputWidth = width;
  int inputHeight = height;
  int outputWidth = width;
  int outputHeight = height;

  int inputSize = inputWidth * inputHeight;
  int outputSize = outputWidth * outputHeight;

  auto inputBuffer = new char[inputSize];
  auto outputBuffer = new char[outputSize];

  struct timespec tp0, tp1, tp2;

  Mat input(inputHeight, inputWidth, CV_8UC1, const_cast<void*>(reinterpret_cast<const void*>(inputBuffer)));
  Mat output(outputHeight, outputWidth, CV_8UC1, outputBuffer);

  randu(input, Scalar::all(std::numeric_limits<Pixel>::min()), Scalar::all(std::numeric_limits<Pixel>::max()));

  UMat in;
  UMat out;

  input.copyTo(in);
  output.copyTo(out);

  // General square mean filter
  /*
  void cv::boxFilter(InputArray  src,
                     OutputArray dst,
                     int         ddepth,
                     Size        ksize,
                     Point       anchor = Point(-1,-1),
                     bool        normalize = true,
                     int         borderType = BORDER_DEFAULT
  )
  */

  clock_gettime(CLOCK_MONOTONIC, &tp0);

  // 3
  sqrBoxFilter(in, out, -1, Size(3, 3));

  clock_gettime(CLOCK_MONOTONIC, &tp1);

  // 7
  sqrBoxFilter(in, out, -1, Size(7, 7));

  clock_gettime(CLOCK_MONOTONIC, &tp2);

  printf("sqrBoxFilter 3x3   -> tdiff=%lf ms \n", tdiff_calc(tp0, tp1));
  printf("sqrBoxFilter 7x7   -> tdiff=%lf ms \n", tdiff_calc(tp1, tp2));

  return 0;
}
