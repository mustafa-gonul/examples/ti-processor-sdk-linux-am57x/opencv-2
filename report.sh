#!/bin/bash

REPORT_FILE=report.txt

make run 2>&1 | tee ${REPORT_FILE}

less -r ${REPORT_FILE}
