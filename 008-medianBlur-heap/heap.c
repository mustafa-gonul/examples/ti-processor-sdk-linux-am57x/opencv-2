/*-----------------------------------------------------------------------------
* User-controlled DSP heaps are initialized within a target region. The call
* to __heap_init_xxx can be included within any target region. However the
* initialization function must be called before any __malloc_xxx calls are
* made.
*
* User-controlled DSP heaps can be persistent across target regions as long as
* the underlying memory (aka buffers pointed to by p are not deallocated.
*----------------------------------------------------------------------------*/
#include <stddef.h>

#pragma omp declare target
extern void __heap_init_ddr(void *ptr, int size);
extern void __heap_init_msmc(void *ptr, int size);
extern int printf(const char *_format, ...);
#pragma omp end declare target


void heap_init_ddr(char* p, size_t bytes)
{
#pragma omp target map(to:bytes,p[0:bytes])
  {
    printf("heap_init_ddr\n");
     __heap_init_ddr(p,bytes);
  }
}


void heap_init_msmc(char *p, size_t bytes)
{
#pragma omp target map(to: bytes, p[0:bytes])
  {
    printf("heap_init_msmc\n");
    __heap_init_msmc(p,bytes);
  }
}
