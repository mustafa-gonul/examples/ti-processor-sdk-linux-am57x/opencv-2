#pragma once

#include <ti_omp_device.h>
#include <stddef.h>


#ifdef __cplusplus
extern "C" {
#endif


void heap_init_ddr(char* __restrict__ p, size_t bytes);
void heap_init_msmc(char* __restrict__ p, size_t bytes);


#ifdef __cplusplus
}
#endif
