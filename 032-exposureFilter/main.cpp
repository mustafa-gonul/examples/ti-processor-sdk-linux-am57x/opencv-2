#include <opencv2/opencv.hpp>
#include <vector>
#include <limits>
#include <time.h>
#include <unistd.h>


using namespace cv;

using Pixel = unsigned char;

/* Time difference calculation, in ms units */
double tdiff_calc(struct timespec &tp_start, struct timespec &tp_end)
{
  return (double)(tp_end.tv_nsec -tp_start.tv_nsec) * 0.000001 + (double)(tp_end.tv_sec - tp_start.tv_sec) * 1000.0;
}

void exposureFilter(Mat input, Mat output)
{
  setNumThreads(0);

  auto uInput = input.getUMat(ACCESS_READ);

  int xRadius = 15;
  int yRadius = 15;
  unsigned ratio = 60;

  Rect inputRect{0, 0, input.cols, input.rows};

  std::vector<int> channels = {0};
  std::vector<int> sizes = {256};
  std::vector<float> ranges = {0, 256};

  // Algorithm
  input.forEach<Pixel>([&](Pixel current, const int position[]) {
    int y = position[0];
    int x = position[1];

    Rect selection{x - xRadius, y - yRadius, xRadius * 2 + 1, yRadius * 2 + 1};
    selection &= inputRect;
    auto count = (unsigned) selection.width * selection.height;
    auto limit = count * ratio / 100;

    UMat uSubInput{uInput, selection};
    std::vector<UMat> images = {uSubInput};
    Mat histogram;
    calcHist(images, channels, UMat(), histogram, sizes, ranges);

    unsigned sum = 0;
    unsigned value = 0;
    for (int i = 0; i < 256; ++i) {
      sum += histogram.at<float>(0, i);
      if (sum >= limit) {
        value = i;
        break;
      }
    }

    output.at<Pixel>(y, x) = saturate_cast<Pixel>(current * 255 / value);
  });
}

int main(int argc, const char** argv)
{
  int width = 21;
  int height = 21;

  if (argc != 3) {
    std::cout << "Invalid parameters!" << std::endl;

    return -1;
  }

  try {
    width = std::stoi(argv[1]);
    height = std::stoi(argv[2]);

    std::cout << "Width = " << width << std::endl;
    std::cout << "Height = " << height << std::endl;
  }
  catch (const std::exception& e) {
    std::cerr << e.what() << '\n';
    return -2;
  }

  int inputWidth = width;
  int inputHeight = height;
  int outputWidth = width;
  int outputHeight = height;

  int inputSize = inputWidth * inputHeight;
  int outputSize = outputWidth * outputHeight;

  auto inputBuffer = new char[inputSize];
  auto outputBuffer = new char[outputSize];

  struct timespec tp0, tp1, tp2;

  Mat input(inputHeight, inputWidth, CV_8UC1, inputBuffer);
  Mat output(outputHeight, outputWidth, CV_8UC1, outputBuffer);

  randu(input, Scalar::all(std::numeric_limits<Pixel>::min()), Scalar::all(std::numeric_limits<Pixel>::max()));

  clock_gettime(CLOCK_MONOTONIC, &tp0);

  exposureFilter(input, output);

  clock_gettime(CLOCK_MONOTONIC, &tp1);

  printf("exposureFilter   -> tdiff=%lf ms \n", tdiff_calc(tp0, tp1));

  return 0;
}
