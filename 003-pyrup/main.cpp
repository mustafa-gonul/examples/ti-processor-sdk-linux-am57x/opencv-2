#include <opencv2/opencv.hpp>
#include <limits>
#include <time.h>
#include <unistd.h>


using namespace cv;

using Pixel = unsigned char;

/* Time difference calculation, in ms units */
double tdiff_calc(struct timespec &tp_start, struct timespec &tp_end)
{
  return (double)(tp_end.tv_nsec -tp_start.tv_nsec) * 0.000001 + (double)(tp_end.tv_sec - tp_start.tv_sec) * 1000.0;
}

int main(int argc, const char** argv)
{
  int width = 21;
  int height = 21;

  if (argc != 3) {
    std::cout << "Invalid parameters!" << std::endl;

    return -1;
  }

  try {
    width = std::stoi(argv[1]);
    height = std::stoi(argv[2]);

    std::cout << "Width = " << width << std::endl;
    std::cout << "Height = " << height << std::endl;
  }
  catch (const std::exception& e) {
    std::cerr << e.what() << '\n';
    return -2;
  }

  int inputWidth = width;
  int inputHeight = height;
  int outputWidth = width * 2; // doubles
  int outputHeight = height * 2; // doubles

  int inputSize = inputWidth * inputHeight;
  int outputSize = outputWidth * outputHeight;

  auto inputBuffer = new char[inputSize];
  auto outputBuffer = new char[outputSize];

  struct timespec tp0, tp1, tp2;

  Mat input(inputHeight, inputWidth, CV_8UC1, const_cast<void*>(reinterpret_cast<const void*>(inputBuffer)));
  Mat output(outputHeight, outputWidth, CV_8UC1, outputBuffer);

  randu(input, Scalar::all(std::numeric_limits<Pixel>::min()), Scalar::all(std::numeric_limits<Pixel>::max()));

  UMat in;
  UMat out;

  input.copyTo(in);
  output.copyTo(out);

  // Upsampling
  /*
  void cv::pyrUp(InputArray   src,
                 OutputArray  dst,
                 const Size & dstsize = Size(),
                 int          borderType = BORDER_DEFAULT
  )
  */
  clock_gettime(CLOCK_MONOTONIC, &tp0);

  pyrUp(in, out, Size(outputWidth, outputHeight));

  clock_gettime(CLOCK_MONOTONIC, &tp1);

  pyrUp(in, out, Size(outputWidth, outputHeight));

  clock_gettime(CLOCK_MONOTONIC, &tp2);

  std::cout << "pyrUp # -> tdiff=" << tdiff_calc(tp0, tp1) << " ms" << std::endl;
  std::cout << "pyrUp   -> tdiff=" << tdiff_calc(tp1, tp2) << " ms" << std::endl;

  return 0;
}
