# OpenCV

## Modules

- [x] Core functionality
- [ ] Image processing
  - [x] Image Filtering
  - [ ] Geometric Image Transformations
  - [ ] Miscellaneous Image Transformations
  - [ ] Drawing Functions
  - [ ] ColorMaps in OpenCV
  - [ ] Histograms
  - [ ] Structural Analysis and Shape Descriptors
  - [ ] Motion Analysis and Object Tracking
  - [ ] Feature Detection
  - [ ] Object Detection
  - [ ] C API

## Image Processing

### Filters

| Open CV | Short Explanation |
|-|-|
| Erode | Minimum Filter |
| Dilate | Maximum Filter |
| Pyramid Up | Upsampling |
| Pyramid Down | Downsampling |
| Box Filter | General Mean Filter |
| Square Box Filter | General Square Mean Filter |
| Blur | Mean Filter |
| Median Blur | Median Filter |
| - | - |
| Equalize Histogram | Equalize Histogram |

### Histograms

| Open CV | Short Explanation |
| - | - |
| Equalize Histogram | Equalize Histogram |
