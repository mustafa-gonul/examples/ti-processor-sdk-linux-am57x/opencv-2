#include <opencv2/opencv.hpp>
#include <limits>
#include <time.h>
#include <unistd.h>


using namespace cv;

using Pixel = unsigned char;

/* Time difference calculation, in ms units */
double tdiff_calc(struct timespec &tp_start, struct timespec &tp_end)
{
  return (double)(tp_end.tv_nsec -tp_start.tv_nsec) * 0.000001 + (double)(tp_end.tv_sec - tp_start.tv_sec) * 1000.0;
}

int main(int argc, const char** argv)
{
  int width = 21;
  int height = 21;

  if (argc != 3) {
    std::cout << "Invalid parameters!" << std::endl;

    return -1;
  }

  try {
    width = std::stoi(argv[1]);
    height = std::stoi(argv[2]);

    std::cout << "Width = " << width << std::endl;
    std::cout << "Height = " << height << std::endl;
  }
  catch (const std::exception& e) {
    std::cerr << e.what() << '\n';
    return -2;
  }

  int firstSize = 7;
  int secondSize = 9;

  int inputWidth = width;
  int inputHeight = height;
  int outputWidth = width;
  int outputHeight = height;

  int inputSize = inputWidth * inputHeight;
  int outputSize = outputWidth * outputHeight;

  auto inputBuffer = new char[inputSize];
  auto outputBuffer = new char[outputSize];

  struct timespec tp0, tp1, tp2, tp3;

  Mat input(inputHeight, inputWidth, CV_8UC1, const_cast<void*>(reinterpret_cast<const void*>(inputBuffer)));
  Mat output(outputHeight, outputWidth, CV_8UC1, outputBuffer);

  randu(input, Scalar::all(std::numeric_limits<Pixel>::min()), Scalar::all(std::numeric_limits<Pixel>::max()));

  UMat in;
  UMat out;

  input.copyTo(in);
  output.copyTo(out);

  // Minimum filter
  /*
  void cv::erode(InputArray     src,
                 OutputArray    dst,
                 InputArray     kernel,
                 Point          anchor = Point(-1,-1),
                 int            iterations = 1,
                 int            borderType = BORDER_CONSTANT,
                 const Scalar & borderValue = morphologyDefaultBorderValue()
  )
  */

  clock_gettime(CLOCK_MONOTONIC, &tp0);

  // first_size
  erode(in, out, getStructuringElement(MORPH_RECT, Size(firstSize, firstSize)));

  clock_gettime(CLOCK_MONOTONIC, &tp1);

  // first size
  erode(in, out, getStructuringElement(MORPH_RECT, Size(firstSize, firstSize)));

  clock_gettime(CLOCK_MONOTONIC, &tp2);

  // second size
  erode(in, out, getStructuringElement(MORPH_RECT, Size(secondSize, secondSize)));

  clock_gettime(CLOCK_MONOTONIC, &tp3);

  std::cout << "erode " <<  firstSize << "x" <<  firstSize << " # -> tdiff=" << tdiff_calc(tp0, tp1) << " ms" << std::endl;
  std::cout << "erode " <<  firstSize << "x" <<  firstSize << "   -> tdiff=" << tdiff_calc(tp1, tp2) << " ms" << std::endl;
  std::cout << "erode " << secondSize << "x" << secondSize << "   -> tdiff=" << tdiff_calc(tp2, tp3) << " ms" << std::endl;

  return 0;
}
