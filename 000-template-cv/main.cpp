#include <opencv2/opencv.hpp>
#include <limits>
#include <cstdlib>
#include <time.h>
#include <unistd.h>

#include <ti_heap.h>


using namespace cv;

using Pixel = unsigned char;

/* Time difference calculation, in ms units */
double tdiff_calc(struct timespec &tp_start, struct timespec &tp_end)
{
  return (double)(tp_end.tv_nsec -tp_start.tv_nsec) * 0.000001 + (double)(tp_end.tv_sec - tp_start.tv_sec) * 1000.0;
}

void setenv()
{
  putenv("TI_OCL_KEEP_FILES=Y");
  putenv("TI_OCL_LOAD_KERNELS_ONCHIP=Y");
  putenv("TI_OCL_CACHE_KERNELS=Y");
  putenv("TI_OCL_VERBOSE_ERROR=Y");

  putenv("OPENCV_OPENCL_DEVICE=TI AM57:ACCELERATOR:TI Multicore C66 DSP");

  std::cout << " +- TI_OCL_KEEP_FILES: " << getenv("TI_OCL_KEEP_FILES") << std::endl;
  std::cout << " +- TI_OCL_LOAD_KERNELS_ONCHIP: " << getenv("TI_OCL_LOAD_KERNELS_ONCHIP") << std::endl;
  std::cout << " +- TI_OCL_CACHE_KERNELS: " << getenv("TI_OCL_CACHE_KERNELS") << std::endl;
  std::cout << " +- TI_OCL_VERBOSE_ERROR: " << getenv("TI_OCL_VERBOSE_ERROR") << std::endl;
  std::cout << " +- OPENCV_OPENCL_DEVICE: " << getenv("OPENCV_OPENCL_DEVICE") << std::endl;
}

bool checkZero(const Pixel* buffer, int size)
{
  for (int i = 0; i < size; ++i) {
    if (buffer[i]) {
      return false;
    }
  }

  return true;
}

void writeZero(Pixel* buffer, int size)
{
  for (int i = 0; i < size; ++i) {
    buffer[i] = 0;
  }
}

int main(int argc, const char** argv)
{
  int width = 21;
  int height = 21;

  if (argc != 3) {
    std::cout << "Invalid parameters!" << std::endl;

    return -1;
  }

  try {
    width = std::stoi(argv[1]);
    height = std::stoi(argv[2]);

    std::cout << "Width = " << width << std::endl;
    std::cout << "Height = " << height << std::endl;
    std::cout << "---" << std::endl;
  }
  catch (const std::exception& e) {
    std::cerr << e.what() << '\n';
    return -2;
  }

  int size = width * height;
  
  // auto inputBuffer = new Pixel[size]{0};
  // auto outputBuffer = new Pixel[size]{0};
  auto inputBuffer = (Pixel*) __malloc_ddr(size * sizeof(Pixel));
  auto outputBuffer = (Pixel*) __malloc_ddr(size * sizeof(Pixel));
  writeZero(inputBuffer, size);
  writeZero(outputBuffer, size);

  if (checkZero(outputBuffer, size)) {
    printf("Zero, Ok\n");
  }
  else {
    printf("Not Zero, Not Ok!\n");
    return -1;
  }

  struct timespec tp0, tp1, tp2, tp3;

  Mat input(height, width, CV_8UC1, inputBuffer);
  Mat output(height, width, CV_8UC1, outputBuffer);

  randu(input, Scalar::all(std::numeric_limits<Pixel>::min()), Scalar::all(std::numeric_limits<Pixel>::max()));
  

  UMat in;
  UMat out;

  clock_gettime(CLOCK_MONOTONIC, &tp0);

  // input.copyTo(in);
  in = input.getUMat(ACCESS_RW);

  clock_gettime(CLOCK_MONOTONIC, &tp1);

  // output.copyTo(out);
  out = output.getUMat(ACCESS_RW);

  clock_gettime(CLOCK_MONOTONIC, &tp2);

  in.copyTo(out);

  clock_gettime(CLOCK_MONOTONIC, &tp3);

  bool equalFlag = true;
  for (unsigned i = 0; i < size; ++i) {
    if (inputBuffer[i] != outputBuffer[i]) {
      equalFlag = false;
      break;
    }
  }
  if (equalFlag) {
    printf("Equal!\n");
  }
  else {
    printf("Not Equal!\n");
  }

  printf("---\n");
  printf("in = input.getUMat(ACCESS_RW);   -> tdiff=%lf ms \n", tdiff_calc(tp0, tp1));
  printf("out = output.getUMat(ACCESS_RW); -> tdiff=%lf ms \n", tdiff_calc(tp1, tp2));
  printf("in.copyTo(out);                  -> tdiff=%lf ms \n", tdiff_calc(tp2, tp3));

  return 0;
}
